/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.restapi.country;

/**
 *
 * @author Prajwal
 */

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;


/**
*
* @author Dell
*/
@Entity
//@Data
@Table(name = "c_country")
@NamedQueries({
@NamedQuery(name = "CCountry.findAll", query = "SELECT c FROM Country c")
,
@NamedQuery(name = "CCountry.findByCountryCode", query = "SELECT c FROM Country c where c.countryCode = :countryCode")
,
@NamedQuery(name = "CCountry.searchByCountryEdesc", query = "SELECT c FROM Country c where UPPER(c.countryEdesc) LIKE :countryEdesc")
//@NamedQuery(name = "CCountry.searchByRegionId", query = "SELECT c FROM Country c where c.regionId.regionId=:regionId")
})
public class Country{

@Id
@Column(name = "COUNTRY_ID")
private Integer countryId;

@Column(name = "COUNTRY_CODE")
private String countryCode;

@Column(name = "COUNTRY_EDESC")
private String countryEdesc;

@Column(name = "COUNTRY_NDESC")
private String countryNdesc;

@Column(name = "CREATED_BY")
private String createdBy;

@Column(name = "CREATED_ON")
@Temporal(TemporalType.DATE)
private Date createdOn;

//@JoinColumn(name = "REGION_ID", referencedColumnName = "REGION_ID")
//@ManyToOne
//private CRegion regionId;

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryEdesc() {
        return countryEdesc;
    }

    public void setCountryEdesc(String countryEdesc) {
        this.countryEdesc = countryEdesc;
    }

    public String getCountryNdesc() {
        return countryNdesc;
    }

    public void setCountryNdesc(String countryNdesc) {
        this.countryNdesc = countryNdesc;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

}
