/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.restapi.country;

/**
 *
 * @author Prajwal
 */
import java.util.List;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transaction;
import javax.transaction.Transactional;
import org.eclipse.persistence.internal.jpa.EntityManagerFactoryImpl;

/**
 *
 * @author Dell
 */
@Named("countryService")
public class CountryService {

    //@PersistenceContext
    //private EntityManager em;
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("primary");
    EntityManager em = emf.createEntityManager();

    public List fetchAll() {
        return em.createNamedQuery("CCountry.findAll", Country.class).getResultList();
    }

//for pagination
    public List fetchSome(int pageNumber) {

// int pageNumber = 1;
        int pageSize = 1;
        Query query = em.createNamedQuery("CCountry.findAll", Country.class);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        List countries = query.getResultList();
        return countries;
    }

    public Country fetchById(int requestId) {
        Country country = em.find(Country.class, requestId);
        if (country != null) {
            return country;
        } else {
            return null;
        }
    }

    public Country fetchByCountryCode(String countryCode) {
        Query query = em.createNamedQuery("CCountry.findByCountryCode", Country.class);
        query.setParameter("countryCode", countryCode);
        Country country = (Country) query.getSingleResult();
        if (country != null) {
            return country;
        } else {
            return null;
        }
    }

    public List fetchByCountryEdesc(String cDesc) {
        Query query = em.createNamedQuery("CCountry.searchByCountryEdesc", Country.class);
        query.setParameter("countryEdesc", "%" + cDesc + "%");
        List countries = query.getResultList();
        if (countries != null) {
            return countries;
        } else {
            return null;
        }
    }

    public List fetchByRegionId(int regionId, int pageNumber) {

        int pageSize = 1;
        Query query = em.createNamedQuery("CCountry.searchByRegionId", Country.class);
        query.setParameter("regionId", regionId);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        List countries = query.getResultList();
        if (countries != null) {
            return countries;
        } else {
            return null;
        }
    }

    @Transactional
    public Boolean delete(int requestId) {
        Country country = fetchById(requestId);
        if (country != null) {
            em.remove(country);
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public String updateData(int requestId, Country country) {

        int countryId = country.getCountryId();
        Country country1 = fetchById(requestId);
        if (country1 == null) {

            return "DATA NOT FOUND";
        } else if (country != null && countryId == requestId) {

            String s = "DATA exists";
            em.merge(country);
//em.remove(country1);
//em.flush();
//em.persist(country);
            return s + " " + "DATA Updated";

        } else {
            return "Some Error Occured";
        }

    }

    @Transactional
    public void createData(Country country) {
        /*
         int countryId = country.getCountryId();
         Country country1 = fetchById(countryId);
         if (country1 == null) {
         em.persist(country);
         System.out.println("DATA Added");
         } else {
         //em.getTransaction().begin();

         //em.getTransaction().commit();
         //em.close();
         System.out.println("DATA EXISTS");
         }
         */
        em.getTransaction().begin();
        em.persist(country);
        em.getTransaction().commit();
        em.close();
        emf.close();

    }

}
