/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.restapi.country;

/**
 *
 * @author Prajwal
 */
import java.time.Instant;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Dell
 */
@Path("country")
public class CountryResource {

    //@Inject
    CountryService countryService = new CountryService();

   // @Inject
    //Service service;
    @GET
    @Path("/string")
    @Produces(MediaType.TEXT_HTML)
    public String fetchString() {

        return "Hello from API";
    }

    @GET
    @Path("/fetch")
    @Produces(MediaType.APPLICATION_JSON)
    public List fetch(@QueryParam(value = "pageNum") @DefaultValue("1") int pageNum) {

        if (pageNum == 0) {

            return countryService.fetchSome(1);

        }
        return countryService.fetchSome(pageNum);

    }

//generic
    @GET
    @Path("/fetchall")
    @Produces(MediaType.APPLICATION_JSON)
    public List fetchAll(@QueryParam(value = "pageNum") @DefaultValue("1") int pageNum) {

        return countryService.fetchAll();
    }

//generic
    @GET
    @Path("fetch/{requestId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Country fetchById(@PathParam("requestId") final int requestId) {
        return countryService.fetchById(requestId);
    }

    /*
     @GET
     @Path("fetch/{requestId}")
     @Produces(MediaType.APPLICATION_JSON)
     public CCountry fetchById(@PathParam("requestId") final int requestId) {
     return countryService.fetchById(requestId);
     }
     */
    @GET
    @Path("fetchcode/{countryCode}")
    @Produces(MediaType.APPLICATION_JSON)
    public Country fetchByCountryCode(@PathParam("countryCode") final String countryCode) {
        return countryService.fetchByCountryCode(countryCode);
    }

    @GET
    @Path("fetchc/{cDesc}")
    @Produces(MediaType.APPLICATION_JSON)
    public List fetchByCountryEdesc(@PathParam("cDesc") final String cDesc) {
        return countryService.fetchByCountryEdesc(cDesc.toUpperCase());
    }

    @GET
    @Path("fetchr/{regionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List fetchByRegionId(@PathParam("regionId") final int regionId,
            @QueryParam(value = "pageNum") @DefaultValue("1") int pageNum) {
        if (pageNum == 0) {

            return countryService.fetchByRegionId(regionId, 1);

        }
        return countryService.fetchByRegionId(regionId, pageNum);
    }

    /*
     @POST
     @Path("/create")
     @Consumes(MediaType.APPLICATION_JSON)
     @Produces(MediaType.APPLICATION_JSON)
     public CCountry getCountry() {

     CCountry country = new CCountry();
     country.setCountryId(3);
     country.setCountryEdesc("China");
     country.setCountryCode("3");
     country.setCountryNdesc("China");
     country.setCreatedBy("Admin");
     //country.setCreatedOn(Date.from(Instant.MIN));
     country.setCreatedOn(new Date());
     System.out.println("resource samma");

     countryService.createData(country);
     return country;
     }*/
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Country getCountry() {

        Country country = new Country();
        country.setCountryId(2);
        country.setCountryEdesc("China");
        country.setCountryCode("2");
        country.setCountryNdesc("China");
        country.setCreatedBy("Admin");
//country.setCreatedOn(Date.from(Instant.MIN));
        country.setCreatedOn(new Date());
        System.out.println("resource samma");

        countryService.createData(country);
        return country;
    }


    /* @DELETE
     @Path("delete/{requestId}")
     @Produces(MediaType.APPLICATION_JSON)
     public Boolean delete(@PathParam("requestId") final int requestId) {
     return countryService.delete(requestId);
     }*/
//Generic
    @DELETE
    @Path("delete/{requestId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean delete(@PathParam("requestId") final int requestId) {
        return countryService.delete(requestId);
    }

    @PUT
    @Path("update/{requestId}")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_JSON)
    public String update(@PathParam("requestId") final int requestId, Country country) {
        return countryService.updateData(requestId, country);
    }

    @GET
    @Path("/ob")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getOb() {
        Country country = new Country();
        country.setCountryId(3);
        country.setCountryEdesc("China");
        String s = new String();
        s = "hello";
        return country + " " + s;
    }
}
